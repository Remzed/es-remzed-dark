# Remzed-dark
remzed-dark theme for Emulation Station

Based on: simple-dark (c) Nils Bonenberger - nilsbyte@nilsbyte.de - http://blog.nilsbyte.de/

Theme 'remzed-dark' v1.0 - 13-08-2017

For use with EmulationStation (http://www.emulationstation.org/)

Features : 
- Video Support
- Wheel(marquee) Support
- Boxart Support
- Spotify System
- Youtube System
- Netflix System 


